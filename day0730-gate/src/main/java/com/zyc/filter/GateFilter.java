package com.zyc.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 09:20
 **/
@Configuration
public class GateFilter implements GlobalFilter, Ordered {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /***
    * @Description: 网关拦截器
    * @Param: [exchange, chain]
    * @return: reactor.core.publisher.Mono<java.lang.Void>
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //获取参数
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //ip and url
        String ip = request.getRemoteAddress().getAddress().toString();
        String url = request.getPath().toString();
        //白名单
        if (ip.contains("192.168.135.131")){
            //ip 放行\
           return  chain.filter(exchange);
        }
        if (url.contains("/user/user/login")){
            //登入放行
          return  chain.filter(exchange);
        }
        //获取token
        String token = request.getHeaders().getFirst("token");
        if (token==null){
            //token 不存在 返回 401异常体
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return  response.setComplete();
        }
        //token 存在说明 用户已经登入 直接全部方形 实现一处认证处处登录 功能
        return chain.filter(exchange);
    }
    /***
    * @Description: 优先级
    * @Param: []
    * @return: int
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @Override
    public int getOrder() {
        //优先级  数字越低优先级越高
        return 0;
    }
}
