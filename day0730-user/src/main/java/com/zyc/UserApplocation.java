package com.zyc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 09:31
 **/
@SpringBootApplication
public class UserApplocation {
    public static void main(String[] args) {
        SpringApplication.run(UserApplocation.class);
    }
}
