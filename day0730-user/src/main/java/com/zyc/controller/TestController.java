package com.zyc.controller;

import com.zyc.pojo.Address;
import com.zyc.pojo.R;
import com.zyc.service.AddressService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author : Hao
 * @author : Mild-mannered soul programmer
 * @date : 2024-07-31 16:47
 * @description :
 **/
@RestController
@RequestMapping("/test/test")
public class TestController {


    @Resource
    private AddressService addressService;

    @RequestMapping("test1")
    public R test1() {
        System.out.println();
        List<Address> list = addressService.list();
        return R.OK(list);
    }

}
