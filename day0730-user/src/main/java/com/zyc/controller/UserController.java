package com.zyc.controller;

import com.zyc.config.FreemarkerUtil;
import com.zyc.pojo.R;
import com.zyc.pojo.User;
import com.zyc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 09:34
 **/
@RestController
@RequestMapping("/user/user")
public class UserController {
    @Autowired
    private UserService userService;
    /*** 
    * @Description: 登入 校验
    * @Param: [user]
    * @return: com.zyc.pojo.R
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @RequestMapping("login")
    public R login(@RequestBody User user){
        return userService.login(user);
    }
}
