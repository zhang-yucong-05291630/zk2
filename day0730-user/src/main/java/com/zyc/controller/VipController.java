package com.zyc.controller;

import com.aliyun.oss.OSS;
import com.zyc.config.FreemarkerUtil;
import com.zyc.config.OSSFileUtil;
import com.zyc.pojo.Address;
import com.zyc.pojo.PageInfoVo;
import com.zyc.pojo.R;
import com.zyc.pojo.Vip;
import com.zyc.service.AddressService;
import com.zyc.service.DepService;
import com.zyc.service.PsService;
import com.zyc.service.VipService;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 10:02
 **/
@RestController
@RequestMapping("/user/vip")
public class VipController {
    @Autowired
    private VipService vipService;

    @Autowired
    private PsService psService;

    @Autowired
    private DepService depService;

    @Autowired
    private AddressService addressService;
    @Autowired
    private OSSFileUtil ossFileUtil;
    /*** 
    * @Description: 查询 会员列表 分页 模糊 
    * @Param: [pageInfoVo]
    * @return: com.zyc.pojo.R
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @RequestMapping("/viplist")
    public R VipList(@RequestBody PageInfoVo pageInfoVo){
    return vipService.selectVipList(pageInfoVo);
    }
    /*** 
    * @Description: 查询 手机号
    * @Param: [tel]
    * @return: com.zyc.pojo.R
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @RequestMapping("selecttel/{tel}")
    public R selectTel(@PathVariable("tel") String tel){
        return vipService.selectTel(tel);
    }
    /***
    * @Description: 客源查询
    * @Param: []
    * @return: com.zyc.pojo.R
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @RequestMapping("/pslist")
    public R PsList(){
        return psService.selectList();
    }
    /***
    * @Description: 部门查询
    * @Param: []
    * @return: com.zyc.pojo.R
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @RequestMapping("/deplist")
    public R DepList(){
        return depService.selectList();
    }
    /***
    * @Description: 三级联动地址查询
    * @Param: []
    * @return: com.zyc.pojo.R
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @RequestMapping("/addresslist")
    public R AddressList( ){
        return addressService.selectList();
    }
    /***
    * @Description: 添加操作
    * @Param:
    * @return:
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @RequestMapping("/add")
    public R add(@RequestBody Vip vip, @RequestHeader("token") String token){
        return vipService.add(vip,token);
    }
    /***
    * @Description: Oss文件上传
    * @Param:
    * @return:
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @RequestMapping("/Addfile")
    public R Addfile(MultipartFile file) throws IOException {
        //使用oss工具类
        String s = ossFileUtil.uploadFile(file);
        return R.OK(s);
    }
    /***
    * @Description: 跟进回显
    * @Param:
    * @return:
    * @Author: Zhang Yucong
    * @Date: 2024/7/30
    */
    @RequestMapping("fu/{id}")
    public R Fu(@PathVariable("id") Integer id){
        return vipService.selectVipdata(id);
    }

    @RequestMapping("freemarker")
    public R FreeMarker() throws TemplateException, IOException {
        HashMap<String, Object> map = new HashMap<>();
        FreemarkerUtil freemarkerUtil = new FreemarkerUtil();
        List<Address> list = addressService.list();
        map.put("list",list);

        String url="C:\\Users\\zyc20\\Desktop\\freemarker\\"+ UUID.randomUUID().toString()+".html";
        freemarkerUtil.createHtml(map,"brand",url);
        return R.OK();
    }
}
