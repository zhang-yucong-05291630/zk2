package com.zyc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyc.pojo.Address;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 11:35
 **/
@Mapper
public interface AddressMapper extends BaseMapper<Address> {
}
