package com.zyc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyc.pojo.Dep;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 11:34
 **/
@Mapper
public interface DepMapper extends BaseMapper<Dep> {
}
