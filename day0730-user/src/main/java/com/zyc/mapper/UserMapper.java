package com.zyc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyc.pojo.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 09:34
 **/
@Mapper
public interface UserMapper  extends BaseMapper<User> {
}
