package com.zyc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyc.pojo.Vip;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 10:02
 **/
@Mapper
public interface VipMapper extends BaseMapper<Vip> {
}
