package com.zyc.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 11:31
 **/
@Data
@TableName("address")
public class Address {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer pid;
    private String addressName;
    @TableField(exist = false)
    private List<Address> children;
}
