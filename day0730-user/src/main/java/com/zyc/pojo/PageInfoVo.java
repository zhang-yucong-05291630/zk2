package com.zyc.pojo;

import lombok.Data;

/**
 * @author 军哥
 * @version 1.0
 * @description: TODO
 * @date 2024/6/5 14:57
 */
@Data
public class PageInfoVo {
    private String keyWord;
    private Integer pageNum;
    private Integer pageSize;
    private String beginTime;
    private String endTime;
}
