package com.zyc.pojo;

import lombok.Data;

/**
 * @author 军哥
 * @version 1.0
 * @description: 返回实体类
 * @date 2024/6/4 16:52
 */

@Data
public class R {
    private Boolean flag;
    private Integer code;
    private String msg;
    private Object data;

    public static R OK() {
        R r = new R();
        r.setFlag(true);
        r.setCode(200);
        r.setMsg("操作成功");
        return r;
    }

    public static R OK(Object data) {
        R r = new R();
        r.setFlag(true);
        r.setCode(200);
        r.setData(data);
        r.setMsg("操作成功");
        return r;
    }

    public static R ERROR(int code, String msg) {
        R r = new R();
        r.setFlag(false);
        r.setCode(code);
        r.setMsg(msg);
        return r;
    }
}
