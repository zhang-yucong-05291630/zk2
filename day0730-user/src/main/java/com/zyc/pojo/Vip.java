package com.zyc.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 09:58
 **/
@TableName("vip")
@Data
public class Vip {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer vipId;
    private String vipName;
    private String vipTel;
    private Integer vipType;
    private Integer vipLv;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private Date vipTime;
    private String vipStore;
    private Integer vipDepId;
    private String vipDepName;
    private Integer vipPsId;
    private String vipPsName;
    private String userName;
}
