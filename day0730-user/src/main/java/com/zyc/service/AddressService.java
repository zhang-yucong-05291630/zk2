package com.zyc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyc.pojo.Address;
import com.zyc.pojo.R;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 11:34
 **/
public interface AddressService extends IService<Address> {
    R selectList();
}
