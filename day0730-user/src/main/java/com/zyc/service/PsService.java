package com.zyc.service;

import com.zyc.pojo.R;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 11:33
 **/
public interface PsService {
    R selectList();
}
