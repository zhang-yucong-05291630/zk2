package com.zyc.service;

import com.zyc.pojo.R;
import com.zyc.pojo.User;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 09:33
 **/
public interface UserService {
    R login(User user);
}
