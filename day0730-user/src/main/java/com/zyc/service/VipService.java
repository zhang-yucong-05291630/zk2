package com.zyc.service;

import com.zyc.pojo.PageInfoVo;
import com.zyc.pojo.R;
import com.zyc.pojo.Vip;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 10:01
 **/
public interface VipService {
    R selectVipList(PageInfoVo pageInfoVo);

    R selectTel(String tel);

    R add(Vip vip, String token);

    R selectVipdata(Integer id);
}
