package com.zyc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyc.mapper.AddressMapper;
import com.zyc.pojo.Address;
import com.zyc.pojo.R;
import com.zyc.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 11:34
 **/
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper,Address> implements AddressService {
    @Autowired
    private AddressMapper addressMapper;
    @Override
    public R selectList() {

        //使用递归的方式进行 三级查询
        List<Address> addresses = addressMapper.selectList(null);
        List<Address> addresses1 = SetChirden(0L, addresses);
        return R.OK(addresses1);
    }

    public List<Address> SetChirden(Long id,List<Address> addressList){
        List<Address> collect = addressList.stream().filter(Address -> Address.getPid() == id.intValue()).collect(Collectors.toList());
        collect.stream().map( Address->{
            List<Address> addresses = SetChirden(Address.getId().longValue(), addressList);
            if (addresses.size()>0){
                Address.setChildren(addresses);
            }
            return addresses;
        }).collect(Collectors.toList());
        return collect;
    };
}
