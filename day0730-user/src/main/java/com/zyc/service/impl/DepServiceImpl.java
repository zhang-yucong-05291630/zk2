package com.zyc.service.impl;

import com.zyc.mapper.DepMapper;
import com.zyc.pojo.Dep;
import com.zyc.pojo.R;
import com.zyc.service.DepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 11:34
 **/
@Service
public class DepServiceImpl implements DepService {
    @Autowired
    private DepMapper depMapper;
    @Override
    public R selectList() {
        List<Dep> deps = depMapper.selectList(null);
        return R.OK(deps);
    }
}
