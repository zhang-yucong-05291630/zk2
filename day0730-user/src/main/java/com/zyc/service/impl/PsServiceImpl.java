package com.zyc.service.impl;

import com.zyc.mapper.PsMapper;
import com.zyc.pojo.Ps;
import com.zyc.pojo.R;
import com.zyc.service.PsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 11:33
 **/
@Service
public class PsServiceImpl implements PsService {
    @Autowired
    private PsMapper psMapper;
    @Override
    public R selectList() {
        List<Ps> ps = psMapper.selectList(null);
        return R.OK(ps);
    }
}
