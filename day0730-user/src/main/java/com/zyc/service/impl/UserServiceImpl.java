package com.zyc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.mapper.UserMapper;
import com.zyc.pojo.R;
import com.zyc.pojo.User;
import com.zyc.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 09:33
 **/
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public R login(User user) {
        //用户名校验
        User userod = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getUsername, user.getUsername()));
        if (userod==null){
            return  R.ERROR(404,"用户名为空");
        }
        //密码校验
        if (userod.getPassword().equals(user.getPassword())){
            //生成token
            String token = Jwts.builder().signWith(SignatureAlgorithm.HS256, "123456").claim("userId", userod.getId()).compact();
            return R.OK(token);
        }else{
            return  R.ERROR(500,"密码错误");
        }


    }
}
