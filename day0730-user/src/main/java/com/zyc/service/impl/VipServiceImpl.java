package com.zyc.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyc.mapper.DepMapper;
import com.zyc.mapper.PsMapper;
import com.zyc.mapper.UserMapper;
import com.zyc.mapper.VipMapper;
import com.zyc.pojo.*;
import com.zyc.service.VipService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author: zyc
 * @create: 2024-07-30 10:02
 **/
@Service
public class VipServiceImpl implements VipService {
    @Autowired
    private VipMapper vipMapper;
    @Autowired
    private PsMapper psMapper;
    @Autowired
    private DepMapper depMapper;
    @Autowired
    private UserMapper userMapper;
    @Override
    public R selectVipList(PageInfoVo pageInfoVo) {
        //创建条件查询
        QueryWrapper<Vip> wrapper = new QueryWrapper<>();
        //查询 关键字
        if (StringUtils.isNotEmpty(pageInfoVo.getKeyWord())){

            wrapper.lambda().eq(Vip::getVipId,pageInfoVo.getKeyWord());

        }
        Page<Vip> vipPage = new Page<>(pageInfoVo.getPageNum(), pageInfoVo.getPageSize());
        Page<Vip> vipPage1 = vipMapper.selectPage(vipPage, wrapper);

        return R.OK(vipPage1);
    }

    @Override
    public R selectTel(String tel) {
        //根据手机号查询 是否存在
        List<Vip> vips = vipMapper.selectList(new QueryWrapper<Vip>().lambda().eq(Vip::getVipTel, tel));
        if (vips.size()>0){
            return R.ERROR(500,"已存在");
        }else{
            return R.OK("不存在");
        }
    }

    @Override
    public R add(Vip vip, String token) {
        String s = RandomUtil.randomNumbers(5);
        vip.setVipId(Integer.valueOf(s));
        Integer integer = Jwts.parser().setSigningKey("123456").parseClaimsJws(token).getBody().get("userId", Integer.class);
        User user = userMapper.selectById(integer);
        vip.setUserName(user.getUsername());
        vip.setVipName(user.getUsername());
        Ps ps = psMapper.selectById(vip.getVipPsId());
        vip.setVipPsName(ps.getPsName());
        Dep dep = depMapper.selectById(vip.getVipDepId());
        vip.setVipDepName(dep.getDepName());
        vipMapper.insert(vip);
        return R.OK();
    }

    @Override
    public R selectVipdata(Integer id) {
        //根据id查询 数据
        Vip vip = vipMapper.selectById(id);
        return R.OK(vip);
    }
}
